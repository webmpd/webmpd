statusInterval = null

$ ->
  $("#control-connect").click connect
  $("#control-disconnect").click disconnect
  $("#control-play").click play
  $("#control-pause").click pause
  $("#control-next").click next
  $("#control-previous").click previous

  $("#player audio").get(0).addEventListener "error", ->
    $("#player audio").get(0).load();
    $("#player audio").get(0).play();

  statusInterval = setInterval updateStatus, 2000

connect = ->
  $.get($SCRIPT_ROOT + "/connect")
    .done (res) -> setStatusString res
    .fail (res) -> alert JSON.stringify res

disconnect = ->
  $.get($SCRIPT_ROOT + "/disconnect")
    .done (res) -> setStatusString res
    .fail (res) -> alert JSON.stringify res

updateStatus = ->
  $.post($SCRIPT_ROOT + "/status")
    .done (res) -> $("#status").html "<p>#{res}</p>"

setStatusString = (s) ->
  clearInterval(statusInterval)
  $("#status").html "<p>#{s}</p>"
  setTimeout (-> statusInterval = setInterval(updateStatus, 2000)), 4000

play = ->
  $.get($SCRIPT_ROOT + "/control/play")
    .done (res) ->
      $("#player audio").get(0).load()
      $("#player audio").get(0).play()

    .fail (res) -> alert JSON.stringify res

pause = ->
  $.get($SCRIPT_ROOT + "/control/pause")
    .done (res) -> $("#player audio").get(0).pause()

next = ->
  $.get($SCRIPT_ROOT + "/control/next")
    .done (res) -> setStatusString res
    .fail (res) -> alert JSON.stringify res

previous = ->
  $.get($SCRIPT_ROOT + "/control/previous")
    .done (res) -> setStatusString res
    .fail (res) -> alert JSON.stringify res

