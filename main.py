from flask import Flask, render_template, request
from util import if_connected
import mpd
import json
import sys
import config

app = Flask(__name__)
mpdclient = None

@app.route("/")
def home():
    return render_template( "index.html"
                          , listen_server=config.listen_server
                          , listen_port=config.listen_port)

@app.route("/connect")
def mpd_connect():
    global mpdclient

    if mpdclient is None:
        mpdclient = mpd.MPDClient()

    mpdclient.timeout = 10
    mpdclient.idletimeout = None

    try:
        mpdclient.connect(config.control_server, config.control_port)
    except mpd.ConnectionError:
        return json.dumps({"status": "error"
            , "type": "Connection Error"
            , "msg": "Cannot connect: is it already connected?"
            })

    return json.dumps({"status": "success"})

@app.route("/disconnect")
def mpd_disconnect():
    return if_connected(mpdclient, "disconnect")

@app.route("/control/<ctl>")
def mpd_control(ctl):
    if ctl in ["play", "pause", "stop", "next", "previous"]:
        return if_connected(mpdclient, ctl)
    else:
        return json.dumps({ "status": "error"
                          , "type": "Unrecognized MPD option"
                          , "value": "Option %s is not recognized" % ctl
                          })

@app.route("/req", methods=["POST"])
def requestList():
    try:
        if request.form["mode"] == "queue":
            return json.dumps(list(map(lambda x: {"file": x["file"], "songid": x["id"]},
                                        mpdclient.playlistinfo())))

        elif request.form["mode"] == "file":
            p = request.form["path"]
            p = p[:-1] if p[-1] == "/" else p

            l = mpdclient.lsinfo(p)
            res = []

            res.extend(map(lambda y: {"file": y["directory"] + "/"},
                           filter(lambda x: "directory" in x,
                                  l)))
            res.extend(map(lambda y: {"file": y["file"]},
                           filter(lambda x: "file" in x,
                                  l)))

            return json.dumps(res)

        elif request.form["mode"] == "playlist":
            return json.dumps(list(map(lambda x: {"file": x["playlist"]},
                                       mpdclient.listplaylists())))
        else:
            return json.dumps(["Could not match 'mode'"])

    except (mpd.ConnectionError, AttributeError):
        return json.dumps({ "status": "error"
                          , "type": "Connection Error"
                          , "msg": str(sys.exc_info()[1])
                          })

@app.route("/add", methods=["POST"])
def add():
    try:
        if request.form["mode"] == "playlist":
            mpdclient.load(request.form["name"])
        elif request.form["mode"] == "file":
            p = request.form["name"]
            p = p[:-1] if p[-1] == "/" else p
            mpdclient.add(p)
    except (mpd.ConnectionError, AttributeError):
        return json.dumps({ "status": "error"
                          , "type": "Connection Error"
                          , "msg": str(sys.exc_info()[1])
                          })

    return json.dumps({ "status": "success" })

def clear():
    try:
        mpdclient.clear()
    except (mpd.ConnectionError, AttributeError):
        return json.dumps({ "status": "error"
                          , "type": "Connection Error"
                          , "msg": str(sys.exc_info()[1])
                          })

@app.route("/delete", methods=["POST"])
def deleteFromQueue():
    try:
        print("delete %s" % request.form["songid"])
        mpdclient.deleteid(int(request.form["songid"]))
    except (mpd.ConnectionError, AttributeError):
        return json.dumps({ "status": "error"
                          , "type": "Connection Error"
                          , "msg": str(sys.exc_info()[1])
                          })

    return json.dumps({ "status": "success" })

@app.route("/status", methods=["POST"])
def status():
    try:
        s = mpdclient.status()

        if s["state"] == "stop":
            title = "--"
        else:
            title = "%s - %s" % ( mpdclient.playlistid(s["songid"])[0].get("artist", "Unknown")
                                , mpdclient.playlistid(s["songid"])[0].get("title", "Unknown")
                                )

        return ("state: %s | volume: %s | repeat: %s | random: %s | song: %s" % 
                (s["state"], s["volume"], s["repeat"], s["random"], title))

    except (mpd.ConnectionError, AttributeError):
        return json.dumps({ "state": "not connected" })

if __name__ == "__main__":
    app.run('0.0.0.0', 5000, debug=True)
