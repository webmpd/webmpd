$ ->
  $("#nav_list").hide()
  $(".nav_item").click -> $("#nav_list").slideToggle(200)

  $("#nav_list .nav_item").click -> 
    $("#nav_current").html $(this).html()

  $("#nav_queue").click -> requestList "/", "queue"
  $("#nav_file").click -> requestList "/", "file"
  $("#nav_playlist").click -> requestList "/", "playlist"

  $("#playlist").on "click", ".playlist-item .action-go", ->
    requestList $(this).parent().attr("data-path"), "file"

  $("#playlist").on "click", ".playlist-item .action-add-file", ->
    addToQueue $(this).parent().attr("data-path"), "file", $(this).parent()

  $("#playlist").on "click", ".playlist-item .action-delete-queue", ->
    deleteFromQueue $(this).parent().attr("data-songid"), "queue", $(this).parent()

  $("#playlist").on "click", ".playlist-item .action-add-playlist", ->
    addToQueue $(this).parent().attr("data-path"), "playlist", $(this).parent()

requestList = (p, m) ->
  $.post(
    $SCRIPT_ROOT + "/req"
    mode: m, path: p
    (res) ->
      $("#breadcrumb-text").val p
      setList res, m
    "json"
  )

setList = (res, m) ->
  $("#playlist ul").html ""

  if res.length == 0
    $("#playlist ul").append "<li><label>No items</label></li>"
  else
    if m == "queue"
      setQueueList res
    else if m == "file"
      setFileList res
    else if m == "playlist"
      setPlayList res
    else
      alert "ERROR: mode not recognized"

setQueueList = (l) ->
  $.each(l, (i, item) ->
    $("#playlist ul").append """
                             <li class="playlist-item"
                                 id="playlist-item-#{i}"
                                 data-path="#{item['file']}"
                                 data-songid="#{item['songid']}">
                               <img class="action-delete-queue"
                                    src="#{$SCRIPT_ROOT}/static/images/SVG/subtract.svg" />
                               <label>#{item['file']}</label>
                             </li>
                             """
  )

setFileList = (l) ->
  $.each(l, (i, item) ->
    $("#playlist ul").append """
                              <li class="playlist-item"
                                  id="playlist-item-#{i}"
                                  data-path="#{item['file']}">
                                <img class="action-add-file"
                                    src="#{$SCRIPT_ROOT}/static/images/SVG/add.svg" />
                                <label class="action-go">#{item['file']}</label>
                              </li>
                              """
  )

setPlayList = (l) ->
  $.each(l, (i, item) ->
    $("#playlist ul").append """
                             <li class="playlist-item"
                                 id="playlist-item-#{i}"
                                 data-path="#{item['file']}"
                                 data-songid="#{item['songid']}">
                               <img class="action-add-playlist"
                                    src="#{$SCRIPT_ROOT}/static/images/SVG/add.svg" />
                               <label>#{item['file']}</label>
                             </li>
                             """
  )

addToQueue = (p, m, elem) ->
  $.post(
    $SCRIPT_ROOT + "/add"
    mode: m, name: p
  ).done((res) ->
    elem.animate({opacity: 0.5}, 250)
  )

deleteFromQueue = (sid, m, elem) ->
  $.post(
    $SCRIPT_ROOT + "/delete"
    songid: sid
    'json'
  ).done((res) ->
    elem.hide(200)
  )

extractProperty = (l, prop, a) -> (x[prop] if x[prop]? for x in l)

