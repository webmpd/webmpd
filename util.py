# decorators
import mpd
import json
import sys

# client: mpd client
# control: the function to be called as string
def if_connected(client, control, *args, **kwargs):
  try:
    getattr(client, control)(*args, **kwargs)
  except (mpd.ConnectionError, AttributeError):
    return json.dumps({ "status": "error"
                      , "type": sys.exc_info()[0].__name__
                      , "msg": str(sys.exc_info()[1])
                      })

  return json.dumps({ "status": "success" })
